import { Component, OnInit } from '@angular/core';
import { FormlyFormOptions, FormlyFieldConfig } from '@ngx-formly/core';
import { FormArray, FormGroup } from '@angular/forms';

import { MediaService } from '../services/media.service';

export interface StepType {
  label: string;
  fields: FormlyFieldConfig[];
}

@Component({
  selector: 'app-data-entry',
  templateUrl: './data-entry.component.html',
  styleUrls: ['./data-entry.component.css']
})

export class DataEntryComponent implements OnInit {

  steps: StepType[] = [
    {
      label: 'Country Profile',
      fields: [
        {
          key: 'country',
          type: 'select',
          templateOptions: {
            label: 'Choose Country',
            required: true,
            options: [
              { label: 'Botswana', value: 'botswana' },
              { label: 'Kenya', value: 'kenya' },
            ],
          },

        },
        {
          key: 'language',
          type: 'select',
          templateOptions: {
            label: 'Choose Language',
            multiple: true,
            required: true,
            options: [
              { label: 'English', value: 'english' },
              { label: 'Setswana', value: 'setswana' },
            ]
          },
        },
        {
          key: 'city',
          type: 'select',
          templateOptions: {
            label: 'Choose City/Town',
            required: true,
            options: [
              { label: 'Gaborone', value: 'gaborone' },
              { label: 'FrancisTown', value: 'francisTown' },
              { label: 'Molepolole', value: 'molepolole' },
              { label: 'Mogodisthane', value: 'Mogodisthane' },
              { label: 'Serewe', value: 'Serewe' }
            ]
          },
        },
        {
          key: 'population',
          type: 'input',
          templateOptions: {
            type: 'number',
            label: 'Enter population of city/town',
            required: true,
          },
        },
        {
          key: 'religion',
          type: 'select',
          templateOptions: {
            label: 'Choose Religion',
            required: true,
            options: [
              { label: 'Christianity', value: 'Christianity' },
              { label: 'Buddhism', value: 'Buddhism' },
              { label: 'Islam', value: 'Islam' },
              { label: 'Muslim ', value: 'Muslim ' },
            ],
          },
        },
        {
          key: 'tv_channel',
          type: 'input',
          templateOptions: {
            label: 'Enter TV channel Name',
            required: true,
          },
        },
        {
          key: 'file',
          type: 'file',
          templateOptions: {
           multiple: true
          }        
         }
      ],
    },
    {
      label: 'Channel Detail',
      fields: [
        {
          key: 'country',
          type: 'select',
          templateOptions: {
            label: 'Choose Country',
            required: true,
            options: [
              { label: 'Botswana', value: 'botswana' },
              { label: 'Kenya', value: 'kenya' },
            ],
          }
        },
        {
          key: 'tv_media_type',
          type: 'select',
          templateOptions: {
            label: 'Choose Media Type',
            required: true,
            options: [
              { label: 'TV', value: 'TV' }
            ],
          }
        },
        {
          key: 'tv_media_name',
          type: 'input',
          templateOptions: {
            label: 'Enter Media Name',
            required: true,
          }
        },
        {
          key: 'tv_overall_genre',
          type: 'select',
          templateOptions: {
            label: 'Choose Overall Genre',
            multiple: true,
            required: true,
            options: [
              { label: 'Sport', value: 'sport' },
              { label: 'General Entertainment', value: 'entertainment' },
              { label: 'LifeStyle', value: 'lifestyle' },
              { label: 'Movie', value: 'movie' },
            ],
          }
        },
        {
          key: 'tv_coverage',
          type: 'select',
          templateOptions: {
            label: 'Choose Coverage Area',
            required: true,
            options: [
              { label: 'Regional', value: 'regional' },
              { label: 'National', value: 'national' },
            ],
          }
        },
        {
          key: 'tv_distribution_platform',
          type: 'select',
          templateOptions: {
            label: 'Choose Distribution Platform',
            required: true,
            options: [
              { label: 'FTAC (Terrestrial)', value: 'terrestrial' },
              { label: 'FTAC (Satellite)', value: 'satellite' },
            ],
          }
        },
        {
          key: 'tv_media_language',
          type: 'select',
          templateOptions: {
            label: 'Choose TV Media Language',
            multiple: true,
            required: true,
            options: [
              { label: 'English', value: 'english' },
              { label: 'Setswana', value: 'setswana' },
            ],
          }
        },
        {
          key: 'tv_media_owner',
          type: 'select',
          templateOptions: {
            label: 'Choose Media Owner',
            required: true,
            options: [
              { label: 'YMH Group', value: 'YMH_Group' },
            ],
          }
        },
        {
          key: 'tv_territories_covered',
          type: 'select',
          templateOptions: {
            label: 'Choose From List Of Country (TV Territories covered)',
            required: true,
            options: [
              { label: 'Botswana', value: 'botswana' },
              { label: 'Kenya', value: 'kenya' },
            ],
          }
        },
        {
          key: 'tv_channel_audience_reach',
          type: 'input',
          templateOptions: {
            label: 'Enter Approx Audience Reach',
            required: true
          }
        },
        // TV territory covered and audience reach can be added
      ],
    },
    {
      label: 'TV Program details',
      fields: [
        {
          key: 'tv_program_title',
          type: 'input',
          templateOptions: {
            label: 'Enter Programme Title',
            required: true,
          },
        },
        {
          key: 'age_classification',
          type: 'select',
          templateOptions: {
            label: 'Choose Age Group',
            required: true,
            multiple: true,
            options: [
              { label: '18 - 25', value: '20-35' },
              { label: '26 - 35', value: '26-35' },
              { label: '36 - 45', value: '36-55' },
              { label: '46 - 55', value: '46-55' },
              { label: 'Above 56', value: '56' },
            ],
          },
        },
        {
          key: 'tv_program_language',
          type: 'select',
          templateOptions: {
            label: 'Choose TV Programme Language',
            multiple: true,
            required: true,
            options: [
              { label: 'English', value: 'english' },
              { label: 'Setswana', value: 'setswana' },
            ]
          }
        },
        {
          key: 'tv_program_genre',
          type: 'select',
          templateOptions: {
            label: 'Choose Genre',
            multiple: true,
            required: true,
            options: [
              { label: 'Sport', value: 'sport' },
              { label: 'General Entertainment', value: 'entertainment' },
              { label: 'LifeStyle', value: 'lifestyle' },
              { label: 'Movie', value: 'movie' },
            ],
          }
        },
        {
          key: 'synopsis',
          type: 'input',
          templateOptions: {
            label: 'Enter synopsis',
            required: true
          }
        },
        {
          key: 'duration',
          type: 'input',
          templateOptions: {
            label: 'Enter Duration',
            required: true
          }
        },
        {
          key: 'day_of_broadcast',
          type: 'input',
          templateOptions: {
            type: 'date',
            label: 'Day of Broadcast',
            required: true,
          },
        }
      ],
    },
    {
      label: 'Target Audience Details',
      fields: [
        {
          key: 'audience_age_group',
          type: 'select',
          templateOptions: {
            label: 'Choose Age Group Of Target Audience',
            required: true,
            multiple: true,
            options: [
              { label: '18 - 25', value: '20-35' },
              { label: '26 - 35', value: '26-35' },
              { label: '36 - 45', value: '36-55' },
              { label: '46 - 55', value: '46-55' },
              { label: 'Above 56', value: '56' },
            ],
          },
        },
        {
          key: 'sex',
          type: 'select',
          templateOptions: {
            label: 'Sex',
            required: true,
            options: [
              { label: 'Male', value: 'male' },
              { label: 'Female', value: 'female' },
            ]
          }
        },
        {
          key: 'hh_income',
          type: 'select',
          templateOptions: {
            label: 'Choose HH income',
            required: true,
            multiple: true,
            options: [
              { label: 'Lower', value: 'lower' },
              { label: 'Middle', value: 'middle' },
              { label: 'Upper', value: 'upper' }
            ],
          }
        },
        {
          key: 'employment_status',
          type: 'select',
          templateOptions: {
            label: 'Select Employment Status',
            required: true,
            options: [
              { label: 'Employed', value: 'employed' },
              { label: 'Unemployed', value: 'unemployed' },
            ]
          }
        },
        {
          key: 'profession',
          type: 'select',
          templateOptions: {
            label: 'Choose Profession',
            required: true,
            options: [
              { label: 'Student', value: 'student' },
              { label: 'Manager', value: 'manager' },
            ]
          }
        },
        {
          key: 'marital_status',
          type: 'select',
          templateOptions: {
            label: 'Choose Your Marital tatus',
            required: true,
            options: [
              { label: 'Single', value: 'single' },
              { label: 'Married', value: 'married' },
            ]
          }
        },
        {
          key: 'family_status',
          type: 'select',
          templateOptions: {
            label: 'Choose Your Family Status',
            required: true,
            options: [
              { label: 'Rich', value: 'rich' },
              { label: 'Middle Class', value: 'middle_class' },
            ]
          }
        }
      ],
    },
    {
      label: 'Broadcast Date/Time of Programme',
      fields: [
        {
          key: 'days_of_original_broadcast',
          type: 'select',
          templateOptions: {
            label: 'Select Original Broadcast Days',
            required: true,
            multiple: true,
            options: [
              { label: 'Monday', value: 'Monday' },
              { label: 'Tuesday', value: 'Tuesday' },
              { label: 'Wednesday', value: 'Wednesday' },
              { label: 'Thursday', value: 'Thursday' },
              { label: 'Friday', value: 'Friday' },
              { label: 'Saturday', value: 'Saturday' },
              { label: 'Sunday', value: 'Sunday' },
            ]
          },
        },
        {
          key: 'times_of_original_broadcast',
          type: 'select',
          templateOptions: {
            label: 'Select Original Broadcast Time',
            required: true,
            multiple: true,
            options: [
              { label: '16:30 - 17:00', value: '16:30 - 17:00' },
              { label: '17:00 - 17:30', value: '17:00 - 17:30' },
              { label: '17:00 - 17:30', value: '17:00 - 17:30' }
            ]
          },
        },
        {
          key: 'time_of_first_repeat',
          type: 'select',
          templateOptions: {
            label: 'Select First Repeat Time',
            multiple: true,
            options: [
              { label: 'Monday - 16:30 - 17:00', value: 'Monday-16:30-17:00' },
              { label: 'Tuesday - 17:00 - 17:30', value: 'Tuesday-17:00-17:30' },
              { label: 'Wednesday - 19:30 - 20:00', value: 'Wednesday-19:30-20:00' }
            ]
          },
        },
        {
          key: 'time_of_second_repeat',
          type: 'select',
          templateOptions: {
            label: 'Select Second Repeat Time',
            multiple: true,
            options: [
              { label: 'Monday - 16:30 - 17:00', value: 'Monday-16:30-17:00' },
              { label: 'Tuesday - 17:00 - 17:30', value: 'Tuesday-17:00-17:30' },
              { label: 'Wednesday - 19:30 - 20:00', value: 'Wednesday-19:30-20:00' }
            ]
          },
        },
        {
          key: 'time_of_third_repeat',
          type: 'select',
          templateOptions: {
            label: 'Select Third Repeat Time',
            multiple: true,
            options: [
              { label: 'Monday - 16:30 - 17:00', value: 'Monday-16:30-17:00' },
              { label: 'Tuesday - 17:00 - 17:30', value: 'Tuesday-17:00-17:30' },
              { label: 'Wednesday - 19:30 - 20:00', value: 'Wednesday-19:30-20:00' }
            ]
          },
        }
      ],
    },
  ];
  form = new FormGroup({});
  model: any = {};
  activeStep = 0;
  options = this.steps.map(() => <FormlyFormOptions>{});
  message: string | undefined;
  loading: boolean | undefined;

  // form = new FormArray(this.steps.map(() => new FormGroup({})));

  constructor(private mediaService: MediaService) {
  }

  ngOnInit(): void {
  }

  prevStep(step: number): void {
    this.activeStep = step - 1;
  }

  nextStep(step: number): void {
    this.activeStep = step + 1;
  }

  submit(): void {
    if (this.form.status === 'INVALID') {
      this.message = 'Please fix the errors before submitting';
    }
    this.loading = true;
    this.mediaService.createMediaRecord(this.model)
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.loading = false;
          this.message = res.message;
          setTimeout(() => {
            this.message = '';
          }, 10000);
          window.scrollTo(0, 0);
        }
      });
  }
}
