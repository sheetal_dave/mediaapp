import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientComponent } from './client/client.component';
import { DataEntryComponent } from './data-entry/data-entry.component';

const routes: Routes = [
  {path: '', component: DataEntryComponent},
  {path: 'client', component: ClientComponent},
  {path: 'client', component: DataEntryComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
