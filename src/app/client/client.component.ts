import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MediaService} from '../services/media.service';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})

export class ClientComponent implements OnInit {

  @ViewChild('htmlData') htmlData: ElementRef | undefined;
  medias: Media[] | undefined;
  searchForm = new FormGroup({
    tv_media_type: new FormControl('', [
      Validators.required
    ]),
    country: new FormControl('', [
      Validators.required
    ]),
    city: new FormControl('', [
      Validators.required
    ]),
    language: new FormControl('', [
      Validators.required
    ]),
    sex: new FormControl(''),
    audience_age_group: new FormControl(''),
    hh_income: new FormControl(''),
    employment_status: new FormControl(''),
    profession: new FormControl(''),
    marital_status: new FormControl(''),
    family_status: new FormControl(''),

  });
  message: string | undefined;
  viewPDF = false;
  pdfSrc = 'assets/Media_Report.pdf';
  loading: boolean | undefined;
  submitted: boolean | undefined;

  constructor(private mediaService: MediaService) {
  }

  ngOnInit(): void {
  }

  get f() {
    return this.searchForm.controls;
  }

  searchMediaRecord(form?: any): void {
    this.submitted = true;
    if (this.searchForm.status === 'INVALID') {
      this.message = 'Please fix the errors before submitting';
      return;
    };
    this.loading = true;
    this.mediaService.searchMediaRecord(this.searchForm.value)
      .subscribe((res: any) => {
        if (res.status === 200) {
          this.medias = res.data;
          this.message = 'Media Record Found Successfully And Preparing Report......';
          setTimeout(() => {
            const aTag = document.createElement('a');
            aTag.setAttribute('id', 'reportPdfId');
            aTag.setAttribute('href', 'assets/Media_report.pdf');
            aTag.setAttribute('download', 'report.pdf');
            aTag.innerText = 'Generate Report';
            aTag.hidden = true;
            aTag.click();
            this.loading = false;
            this.message = '';

          }, 6000);
          //  this.generatePDF();
        }
      });
  }

  generatePDF(): void {
    const data = document.getElementById('htmlData');
    if (data) {
      setTimeout(() => {
        // @ts-ignore
        const options = {
          backgroundImage: new URL('https://homepages.cae.wisc.edu/~ece533/images/watch.png'),
          allowTaint: false,
          useCORS: false,
          multi: true,
          height: data.clientHeight,
          width: data.clientWidth
        };
        html2canvas(data, options).then((canvas) => {
          const imgData = canvas.toDataURL('image/png');

          const imgWidth = 210;
          const pageHeight = 295;
          const imgHeight = canvas.height * imgWidth / canvas.width;
          let heightLeft = imgHeight;
          const doc = new jsPDF('p', 'mm');
          let position = 0;

          doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
          heightLeft -= pageHeight;

          while (heightLeft >= 0) {
            position = heightLeft - imgHeight;
            doc.addPage();
            doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight);
            heightLeft -= pageHeight;
          }
          doc.save('test.pdf');
        });
      }, 1000);
    }
  }

}

interface Media {
  tv_media_name: string;
  tv_program_genre: string;
  duration: string;
  tv_program_language: string;
  day_of_broadcast: string;
  days_of_original_broadcast: string;
  times_of_original_broadcast: string;
  time_of_first_repeat: string;
  sex: string;
  audience_age_group: string;
  hh_income: string;
  synopsis: string;
}
