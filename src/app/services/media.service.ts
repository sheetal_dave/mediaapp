import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MediaService {

  private BASE_URL = environment.BASE_URL;

  constructor(private http: HttpClient) {
  }

  createMediaRecord(body: object): Observable<object> {
    return this.http.post(`${this.BASE_URL}create`, body);
  }

  searchMediaRecord(body: object): Observable<object> {
    return this.http.post(`${this.BASE_URL}search`, body);
  }
}
